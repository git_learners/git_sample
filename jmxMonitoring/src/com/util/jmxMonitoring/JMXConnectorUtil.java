package com.util.jmxMonitoring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class JMXConnectorUtil {
	
	private JMXConnectorProperties jmxConnectorProperties;
	
	public JMXConnectorUtil(JMXConnectorProperties jmxConnectorProperties) {
		this.jmxConnectorProperties = jmxConnectorProperties;
	}
	
/*	public JMXConnector[] connect() {
		JMXServiceURL[] jmxServiceURLs = jmxConnectorProperties.getJMXServiceURLs();
		JMXConnector[] jmxConnectors = new JMXConnector[jmxServiceURLs.length];
		
		int i = 0;
		for (JMXServiceURL jmxServiceURL : jmxServiceURLs) {
			JMXConnector connector = getJMXConnection(jmxServiceURL);
			if (connector != null) {
				jmxConnectors[i] = connector;
				i++;
			}
		}
		
		return jmxConnectors;
	}
*/	
	public ArrayList<JMXConnector> connect() {

		JMXServiceURL[] jmxServiceURLs = jmxConnectorProperties.getJMXServiceURLs();
		ArrayList<JMXConnector> connections = new ArrayList<JMXConnector>();
		
		for (JMXServiceURL jmxServiceURL : jmxServiceURLs) {
			JMXConnector connector = getJMXConnection(jmxServiceURL);
			if (connector != null) {
				connections.add(connector);
			}
		}
		
		return connections;
	}
	
	public void closeJMXConnection(ArrayList<JMXConnector> connectors) {
		
		try {
			for (JMXConnector connector : connectors) {
				if (connector == null) {
					continue;
				}
				connector.close();
				System.out.println("JMX Connection closed....");
			}
			
		} catch (IOException e) {
			System.out.println("Error while closing JMX Connection....");
			e.printStackTrace();
		}
	}


	
	private JMXConnector getJMXConnection(JMXServiceURL jmxServiceURL) {
		
		Map<String, String[]> map = new HashMap<String, String[]>();
		String[] credentials = new String[] { jmxConnectorProperties.getJmxUserName(), jmxConnectorProperties.getJmxPassword()};
		map.put("jmx.remote.credentials", credentials);

		JMXConnector jmxConnector = null;
		try {
			jmxConnector = JMXConnectorFactory.connect(jmxServiceURL, map);
			System.out.println("JMXConnector=" + jmxConnector.toString());
			String id = jmxConnector.getConnectionId();
			System.out.println("Connection Id=" + id);

		} catch (IOException e) {
			System.out.println("IOException while connecting with URL " + jmxServiceURL);
			System.out.println("JMX Connector " + jmxConnector);
			//e.printStackTrace();
		}

		return jmxConnector;
	}
	
}
