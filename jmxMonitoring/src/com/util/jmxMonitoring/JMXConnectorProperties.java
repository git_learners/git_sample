package com.util.jmxMonitoring;

import java.net.MalformedURLException;

import javax.management.remote.JMXServiceURL;

public class JMXConnectorProperties {

	private final String jmxServiceUrlPrefix = "service:jmx:rmi:///jndi/rmi://";
	private final String jmxServiceUrlSuffix = "/jmxrmi";
	private String jmxPort;
	private String jmxUserName;
	private String jmxPassword;
	private String[] servers;
	
	public String getJmxPort() {
		return jmxPort;
	}
	public void setJmxPort(String jmxPort) {
		this.jmxPort = jmxPort;
	}
	public String getJmxServiceUrlPrefix() {
		return jmxServiceUrlPrefix;
	}
	public String getJmxServiceUrlSuffix() {
		return jmxServiceUrlSuffix;
	}
	public String getJmxUserName() {
		return jmxUserName;
	}
	public void setJmxUserName(String jmxUserName) {
		this.jmxUserName = jmxUserName;
	}
	public String getJmxPassword() {
		return jmxPassword;
	}
	public void setJmxPassword(String jmxPassword) {
		this.jmxPassword = jmxPassword;
	}
	public void setServers(String[] servers) {
		this.servers = servers;
	}
	public JMXServiceURL[] getJMXServiceURLs() {
		JMXServiceURL[] serviceURLs = new JMXServiceURL[servers.length];
		int i = 0;
		for (String serverIP : servers) {
			try {
				serviceURLs[i] = new JMXServiceURL(getJmxServiceUrlPrefix() + serverIP + ":" + getJmxPort() + getJmxServiceUrlSuffix());
			} catch (MalformedURLException e) {
				System.out.println("Could not connect to server @" + serverIP);
				e.printStackTrace();
			}
			i++;
		}
		return serviceURLs;
	}
}
