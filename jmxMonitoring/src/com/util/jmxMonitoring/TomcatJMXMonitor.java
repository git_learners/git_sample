package com.util.jmxMonitoring;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ObjectInputStream.GetField;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ThreadPoolExecutor;

import javax.management.AttributeNotFoundException;
import javax.management.Descriptor;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class TomcatJMXMonitor {
	
	private static final String OP_DB_CONN = "DB Connections";
	private static final String OP_TOMCAT_THREADS = "Tomcat Threads";

	private static final String LOG_FILE_PATH = "logs-new/";
	private static final String OP_DB_CONN_FILE_NAME = "ConnectionPoolUsageLog";
	private static final String OP_TOMCAT_THREADS_FILE_NAME = "TomcatThreadPoolUsageLog";
	private static final String DEFAULT_FILE_NAME = "TomcatJMXMonitoring";
	private static final String FILE_SUFFIX = ".csv";
	private static final String COMMA_DELIMITER = ",";
	private static final int FLUSH_INTERVAL = 5 * 60 * 1000; // 5 minutes


	//private static final String[] serverIP = new String[] {"10.34.11.214","10.34.8.200","10.34.8.141","10.34.11.39","10.34.8.139","10.34.11.41","10.34.8.137","10.34.11.42","10.34.8.24","10.34.11.183"}; // HMOF CERT servers
	//private static final String[] serverIP = new String[] {"10.34.11.216","10.34.8.60"}; // HMOF PRV & Reg CERT servers
	//private static final String[] serverIP = new String[] {"10.34.7.236","10.34.7.237","10.34.7.235","10.34.7.238","10.34.10.148","10.34.10.54","10.34.10.51","10.34.10.53"}; // TC CERT Servers
	//private static final String[] serverIP = new String[] {"10.34.8.141"}; // for testing
	//private static final String jmxUserName = "jmxadmin"; // HMOF
	//private static final String jmxPassword = "jmxAdM!n%HM0F01"; // HMOF
	//private static final String jmxUserName = "jmxadmin"; // TC
	//private static final String jmxPassword = "jmxAdM!n%Pwd01"; // TC
	//private static final String jmxPort = "15000";
	
	private String dbConnFileName;
	private String threadPoolFileName;
	
	private FileWriter threadPoolFileWriter;
	private BufferedWriter threadPoolBufferedWriter;
	private PrintWriter threadPoolPrintWriter;

	private FileWriter connPoolFileWriter;
	private BufferedWriter connPoolBufferedWriter;
	private PrintWriter connPoolPrintWriter;


	public static void main(String[] args) {
		
		TomcatJMXMonitor jmxMonitor = new TomcatJMXMonitor();
		Properties appProperties = jmxMonitor.getProperties();
		String servers = appProperties.getProperty("server.ip");
		String jmxUsername = appProperties.getProperty("jmx.username");
		String jmxPassword = appProperties.getProperty("jmx.password");
		String jmxPort = appProperties.getProperty("jmx.port");
		String[] serverIP = jmxMonitor.getServerIP(servers);
		//System.out.println(servers);
		for (String server : serverIP) {
			System.out.println(server);
		}
		JMXConnectorProperties jmxConnectorProperties = new JMXConnectorProperties();
		jmxConnectorProperties.setServers(serverIP);
		jmxConnectorProperties.setJmxUserName(jmxUsername);
		jmxConnectorProperties.setJmxPassword(jmxPassword);
		jmxConnectorProperties.setJmxPort(jmxPort);
		
		JMXConnectorUtil jmxConnectorUtil = new JMXConnectorUtil(jmxConnectorProperties);
		ArrayList<JMXConnector> jmxConnectors = jmxConnectorUtil.connect();
		ArrayList<MBeanServerConnection> connections = jmxMonitor.getMBeanServerConnections(jmxConnectors);
		if (connections == null || connections.size() == 0) {
			System.out.println("Unable to establish JMX connections....");
			return;
		}
		jmxMonitor.createPrintWriter();
		jmxMonitor.writeHeaders(serverIP);
		//jmxMonitor.logActiveDBConnections(connections);
		//jmxMonitor.logActiveTomcatConnections(connections);
		
		long startTime = System.currentTimeMillis();
		int count = 0;
		
		String runTime = appProperties.getProperty("duration.to.monitor");
		String threadSleepPeriod = appProperties.getProperty("interval.to.collect.values");
		if (args.length > 0) {
			runTime = args[0];
		} 
		if (args.length > 1) {
			threadSleepPeriod = args[1];
		}
		int runDuration = Integer.parseInt(runTime) * 60 * 1000;
		int sleepTime = Integer.parseInt(threadSleepPeriod) * 1000;
		
		System.out.println("Monitoring ThreadPool and Connection pool");
		System.out.println("Monitoring runs for " + (runDuration/60000) + " minutes with a sleep time of " + (sleepTime/1000) + " seconds.");
		long lastFlushTime = System.currentTimeMillis();
		while (true) {
			try {
				jmxMonitor.logActiveDBConnections(connections);
				jmxMonitor.logActiveTomcatConnections(connections);
				Thread.sleep(sleepTime);
				if ((System.currentTimeMillis() - lastFlushTime) > FLUSH_INTERVAL) {
					jmxMonitor.threadPoolPrintWriter.flush();
					jmxMonitor.connPoolPrintWriter.flush();
					lastFlushTime = System.currentTimeMillis();
					System.out.println("Flushed the data");
				}
/*				if (count == 3) {
					throw new Exception("Simulated Exception");
				}
*/
			} catch (InterruptedException e) {
				jmxMonitor.closeFile(OP_DB_CONN);
				jmxMonitor.closeFile(OP_TOMCAT_THREADS);
				jmxConnectorUtil.closeJMXConnection(jmxConnectors);
				System.out.println("Process interrupted....");
				e.printStackTrace();
				throw new RuntimeException(e);
				
			} catch (Exception ex) {
				jmxMonitor.closeFile(OP_DB_CONN);
				jmxMonitor.closeFile(OP_TOMCAT_THREADS);
				jmxConnectorUtil.closeJMXConnection(jmxConnectors);
				ex.printStackTrace();
				throw new RuntimeException(ex);
			}
			
			// Just for testing
/*			if (count >= 5) {
				break;
			} else {
				count++;
			}
*/			if ((System.currentTimeMillis() - startTime) > runDuration) {
				break;
			}
		}
		
		//JMXConnector jmxConnector = jmxMonitor.getJMXConnection();
		//MBeanServerConnection mBeanServerConnection = jmxMonitor.getMBeanServerConnection(jmxConnector);
		//jmxMonitor.queryMBeans(mBeanServerConnection);
		//jmxMonitor.queryMbeanAttributes(mBeanServerConnection);
		//jmxMonitor.closeJMXConnection(jmxConnector);
		jmxMonitor.closeFile(OP_DB_CONN);
		jmxMonitor.closeFile(OP_TOMCAT_THREADS);
		jmxConnectorUtil.closeJMXConnection(jmxConnectors);
		System.out.println("Process completed....");
		System.exit(0);
	}

/*	private JMXConnector getJMXConnection() {

		JMXServiceURL url = null;
		try {
			url = new JMXServiceURL(
					"service:jmx:rmi:///jndi/rmi://10.34.8.141:15000/jmxrmi");
		} catch (MalformedURLException e) {
			System.out.println("Malformed URL");
			e.printStackTrace();
		}

		Map<String, String[]> map = new HashMap<String, String[]>();
		String[] credentials = new String[] { "jmxadmin", "jmxAdM!n%HM0F01" };
		map.put("jmx.remote.credentials", credentials);

		JMXConnector conn = null;
		try {
			conn = JMXConnectorFactory.connect(url, map);
			System.out.println("JMXConnector=" + conn.toString());
			String id = conn.getConnectionId();
			System.out.println("Connection Id=" + id);

		} catch (IOException e) {
			System.out.println("IOException while connecting...");
			e.printStackTrace();
		}

		return conn;
	}
	
	private void closeJMXConnection(JMXConnector connector) {
		
		try {
			connector.close();
			System.out.println("JMX Connection closed....");
			
		} catch (IOException e) {
			System.out.println("Error while closing JMX Connection....");
			e.printStackTrace();
		}
	}
*/
	
	private Properties getProperties() {

	    Properties properties = new Properties();
	    String path = "./jmxMonitoring.properties";
	    try {
		    FileInputStream file = new FileInputStream(path);
		    properties.load(file);
		    file.close();
	    	
	    } catch (IOException io) {
	    	System.out.println("Error while loading the properties file...");
	    	io.printStackTrace();
	    	throw new RuntimeException(io);
	    }
	    return properties;
	}
	
	private String[] getServerIP(String servers) {
		
		StringTokenizer stringTokenizer = new StringTokenizer(servers, COMMA_DELIMITER);
		int countOfTokens = stringTokenizer.countTokens();
		System.out.println("Count of Servers: " + countOfTokens);
		String[] serverIP = new String[countOfTokens];
		int index = 0;
		while (stringTokenizer.hasMoreTokens()) {
			String token = stringTokenizer.nextToken();
			serverIP[index] = token;
			index++;
		}
		
		return serverIP;
	}
	
	private void logActiveTomcatConnections(ArrayList<MBeanServerConnection> connections) {
		
		String mBeanAttribute = "connectionCount";
		StringBuilder values = new StringBuilder("");
		boolean isFirst = true;
		ObjectName objectName = getObjNameForThreadPool(connections.get(0));
		for (MBeanServerConnection connection : connections) {
			if (connection == null) {
				continue;
			}
			Object value = null;
			try {
				value = connection.getAttribute(objectName, mBeanAttribute);
				if (isFirst) {
					isFirst = false;
				} else {
					values.append(",");
				}
				values.append(value);

			} catch (AttributeNotFoundException | InstanceNotFoundException
					| MBeanException | ReflectionException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(values);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String time = sdf.format(new Date());
		threadPoolPrintWriter.write(time + "," + values.toString() + "\n");
	}
	
	private void logActiveDBConnections(ArrayList<MBeanServerConnection> connections) {
		
		String mBeanAttribute = "NumActive";
		StringBuilder values = new StringBuilder("");
		boolean isFirst = true;
		ObjectName objectName = getObjNameForDBConnPool(connections.get(0));
		for (MBeanServerConnection connection : connections) {
			if (connection == null) {
				continue;
			}
			Object value = null;
			try {
				value = connection.getAttribute(objectName, mBeanAttribute);
				if (isFirst) {
					values.append(value);
					isFirst = false;
				} else {
					values.append("," + value);
				}

			} catch (AttributeNotFoundException | InstanceNotFoundException
					| MBeanException | ReflectionException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(values);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String time = sdf.format(new Date());
		connPoolPrintWriter.write(time + "," + values.toString() + "\n");
	}
	
	private ArrayList<MBeanServerConnection> getMBeanServerConnections(ArrayList<JMXConnector> connectors) {

		ArrayList<MBeanServerConnection> connections = new ArrayList<MBeanServerConnection>();
		if (connectors != null) {
			for (JMXConnector jmxConnector : connectors) {
				try {
					MBeanServerConnection mbsc = jmxConnector.getMBeanServerConnection();
					connections.add(mbsc);
				} catch (IOException e) {
					System.out.println("IOException while getting MBean Server connection...");
					e.printStackTrace();
				}
			}
			
		}
		
		return connections;
	}

	private ObjectName getObjNameForThreadPool(MBeanServerConnection mbsc) {
		
		if (mbsc == null) {
			return null;
		}
		
		ObjectName objectName = null;
		try {
			//Integer mbeanCount = mbsc.getMBeanCount();
			//System.out.println("mbeanCount : " + mbeanCount.intValue());
			ObjectName threadPoolQuery = new ObjectName("Catalina90:*,type=ThreadPool");
			
			Set<ObjectInstance> mBeanSet = mbsc.queryMBeans(threadPoolQuery, null);
			//System.out.println("mBeanSet.size() : " + mBeanSet.size());
			ObjectInstance objectInstance = mBeanSet.iterator().next();
			objectName = objectInstance.getObjectName();
			
		} catch (MalformedObjectNameException e) {
			System.out.println("Error while creating MBeans query....");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectName;
	}
	
	private ObjectName getObjNameForDBConnPool(MBeanServerConnection mbsc) {
		
		if (mbsc == null) {
			return null;
		}
		
		ObjectName objectName = null;
		try {
			//Integer mbeanCount = mbsc.getMBeanCount();
			//System.out.println("mbeanCount : " + mbeanCount.intValue());
			ObjectName connectionPoolQuery = new ObjectName("tomcat.jdbc:*,name=\"jdbc/LyceaDataSource\",type=ConnectionPool");
			
			Set<ObjectInstance> mBeanSet = mbsc.queryMBeans(connectionPoolQuery, null);
			//System.out.println("mBeanSet.size() : " + mBeanSet.size());
			ObjectInstance objectInstance = mBeanSet.iterator().next();
			objectName = objectInstance.getObjectName();
			
		} catch (MalformedObjectNameException e) {
			System.out.println("Error while creating MBeans query....");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectName;
	}
	
	private void createPrintWriter() {
		
		try {
			createFiles();
			connPoolFileWriter = new FileWriter(dbConnFileName, true);
		    connPoolBufferedWriter = new BufferedWriter(connPoolFileWriter);
		    connPoolPrintWriter = new PrintWriter(connPoolBufferedWriter);

			threadPoolFileWriter = new FileWriter(threadPoolFileName, true);
		    threadPoolBufferedWriter = new BufferedWriter(threadPoolFileWriter);
		    threadPoolPrintWriter = new PrintWriter(threadPoolBufferedWriter);

		} catch (IOException ex) {
			System.out.println("Error while creating PrintWriter...");
			ex.printStackTrace();
			closeFile(OP_DB_CONN);
			closeFile(OP_TOMCAT_THREADS);
		} 
	}
	
	private void createFiles() throws IOException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmm");
		String time = sdf.format(new Date());
		File file = new File(LOG_FILE_PATH);
		file.mkdirs();
		dbConnFileName = LOG_FILE_PATH + OP_DB_CONN_FILE_NAME + time + FILE_SUFFIX;
		threadPoolFileName = LOG_FILE_PATH + OP_TOMCAT_THREADS_FILE_NAME + time + FILE_SUFFIX;
		file = new File(dbConnFileName);
		//file.mkdirs();
		file = new File(threadPoolFileName);
		//file.mkdirs();
	}
	
	private void closeFile(String operation) {
		
		if (operation.equals(OP_DB_CONN)) {
			try {
				if (connPoolPrintWriter != null) {
					connPoolPrintWriter.flush();
					connPoolPrintWriter.close();
				}
				if (connPoolBufferedWriter != null) {
					//connPoolBufferedWriter.flush();
					connPoolBufferedWriter.close();
				}
				if (connPoolFileWriter != null) {
					//connPoolFileWriter.flush();
					connPoolFileWriter.close();
				}
			} catch (IOException ex) {
				System.out.println("Error while closing the conn pool writer file...");
				ex.printStackTrace();
			}
		}

		if (operation.equals(OP_TOMCAT_THREADS)) {
			try {
				if (threadPoolPrintWriter != null) {
					threadPoolPrintWriter.flush();
					threadPoolPrintWriter.close();
				}
				if (threadPoolBufferedWriter != null) {
					//threadPoolBufferedWriter.flush();
					threadPoolBufferedWriter.close();
				}
				if (threadPoolFileWriter != null) {
					//threadPoolFileWriter.flush();
					threadPoolFileWriter.close();
				}
			} catch (IOException ex) {
				System.out.println("Error while closing the thread pool writer file...");
				ex.printStackTrace();
			}
		}
	}
	
	private void writeHeaders(String[] serverIP) {
		
		StringBuilder headerValue = new StringBuilder("Time,");
		boolean isFirst = true;
		for (String server : serverIP) {
			if (isFirst) {
				headerValue.append(server);
				isFirst = false;
			} else {
				headerValue.append("," + server);
			}
		}
		threadPoolPrintWriter.write(headerValue.toString() + "\n");
		connPoolPrintWriter.write(headerValue.toString() + "\n");
	}
	

/*	private void queryMbeanAttributes(MBeanServerConnection mbsc) {
		
		Set<ObjectInstance> mBeans = queryMBeans(mbsc);
		ObjectInstance objectInstance = mBeans.iterator().next();
		ObjectName objectName = objectInstance.getObjectName();
		try {
			//ObjectName connectionPoolQuery = new ObjectName("tomcat.jdbc:*,name=\"jdbc/LyceaDataSource\",type=ConnectionPool");
			MBeanInfo beanInfo = mbsc.getMBeanInfo(objectName);
			MBeanAttributeInfo[] attributes = beanInfo.getAttributes();
			System.out.println("Printing Attribute Info....");

			for (MBeanAttributeInfo attributeInfo : attributes) {
				String name = attributeInfo.getName();
				Object value = mbsc.getAttribute(objectName, name);
				System.out.println(name + " : " + value);
			}
			
		} catch (InstanceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IntrospectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ReflectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedObjectNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AttributeNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MBeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
*/	
/*	private Set<ObjectInstance> queryMBeans(MBeanServerConnection mbsc) {

		Set<ObjectInstance> mBeanSet = null;
		try {
			//Integer mbeanCount = mbsc.getMBeanCount();
			//System.out.println("mbeanCount : " + mbeanCount.intValue());
			//ObjectName connectionPoolQuery = new ObjectName("tomcat.jdbc:*,name=\"jdbc/LyceaDataSource\",type=ConnectionPool");
			ObjectName connectionPoolQuery = new ObjectName("Catalina90:*,type=ThreadPool");
			
			mBeanSet = mbsc.queryMBeans(connectionPoolQuery, null);
			//System.out.println("mBeanSet.size() : " + mBeanSet.size());
			
		} catch (MalformedObjectNameException e) {
			System.out.println("Error while creating MBeans query....");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mBeanSet;

	}
*/	
/*	private void queryMBeans(MBeanServerConnection mbsc) {

		try {
			Integer mbeanCount = mbsc.getMBeanCount();
			System.out.println("mbeanCount : " + mbeanCount.intValue());
			try {
				ObjectName connectionPoolQuery = new ObjectName("tomcat.jdbc:*,name=\"jdbc/LyceaDataSource\",type=ConnectionPool");
				Set<ObjectInstance> mBeanSet = mbsc.queryMBeans(connectionPoolQuery, null);
				System.out.println("mBeanSet.size() : " + mBeanSet.size());
				Object attribute = mbsc.getAttribute(connectionPoolQuery, "numActive");
				System.out.println("Attribute : " + attribute);
			} catch (AttributeNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstanceNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MBeanException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ReflectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedObjectNameException e) {
				System.out.println("Error while creating MBeans query....");
				e.printStackTrace();
			}			
			Iterator<ObjectInstance> mBeanSetIterator = mBeanSet.iterator();
			while (mBeanSetIterator.hasNext()) {
				ObjectInstance objectInstance = mBeanSetIterator.next();
				ObjectName objectName = objectInstance.getObjectName();
				String canonicalName = objectName.getCanonicalName();
				String domain = objectName.getDomain();
				if (domain.contains("tomcat.jdbc"))
					System.out.println("Domain :" + domain + " >>> canonicalName : " + canonicalName);
				//String canonicalKeyPropList = objectName.getCanonicalKeyPropertyListString();
			}

			for (ObjectInstance objectInstance : mBeanSet) {
				ObjectName objectName = objectInstance.getObjectName();
				Hashtable<String, String> mbeanPropsTable = objectName.getKeyPropertyList();
				Set<String> mBeanProps = mbeanPropsTable.keySet();
				System.out.println("Printing properties of " + objectName);
				for (String property : mBeanProps) {
					System.out.println(property);
				}
			}
 
		} catch (IOException e) {
			System.out.println("IOException while getting MBean attributes...");
			e.printStackTrace();
		}

	}
*/
}
